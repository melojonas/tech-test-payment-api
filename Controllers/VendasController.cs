using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly RegistroContext _context;
        private readonly Dictionary<EnumStatusVenda, List<EnumStatusVenda>> _status = new Dictionary<EnumStatusVenda, List<EnumStatusVenda>>
        {
            { EnumStatusVenda.Aguardando,
            new List<EnumStatusVenda> { EnumStatusVenda.Aprovado, EnumStatusVenda.Cancelado }},
            { EnumStatusVenda.Aprovado,
            new List<EnumStatusVenda> { EnumStatusVenda.Enviado, EnumStatusVenda.Cancelado }},
            { EnumStatusVenda.Enviado,
            new List<EnumStatusVenda> { EnumStatusVenda.Entregue }}
        };

        public VendasController(RegistroContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Create(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            
            if (venda == null) { return NotFound(); }

            return Ok(venda);
        }

        
        [HttpGet("ObterPorVendedor")]
        public IActionResult ObterPorNome(int id)
        {
            var vendas = _context.Vendas.Where(v => v.IdPessoaVendedora == id);
            
            return Ok(vendas);
        }

        [HttpPatch]
        public IActionResult AlterarStatus(int id, EnumStatusVenda novoStatus)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null) { return NotFound(); }

            if (!(_status.ContainsKey(venda.Status) && _status[venda.Status].Contains(novoStatus)))
            {
                return BadRequest();
            }

            venda.Status = novoStatus;
            _context.Vendas.Update(venda);
            _context.SaveChanges();

            return Ok(venda);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var venda = _context.Vendas.Find(id);
            
            if (venda == null) { return NotFound(); }

            _context.Vendas.Remove(venda);
            _context.SaveChanges();

            return NoContent();
        }
    }
}