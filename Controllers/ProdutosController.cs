using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProdutosController : ControllerBase
    {
        private readonly RegistroContext _context;

        public ProdutosController(RegistroContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Create(Produto produto)
        {
            _context.Add(produto);
            _context.SaveChanges();
            return Ok(produto);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var produto = _context.Produtos.Find(id);
            
            if (produto == null) { return NotFound(); }

            return Ok(produto);
        }

        
        [HttpGet("ObterPorNome")]
        public IActionResult ObterPorNome(string nome)
        {
            var produtos = _context.Produtos.Where(x => x.Nome.Contains(nome));
            
            return Ok(produtos);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Produto produto)
        {
            var produtoBanco = _context.Produtos.Find(id);

            if (produtoBanco == null) { return NotFound(); }

            produtoBanco.Nome = produto.Nome;
            produtoBanco.Descricao = produto.Descricao;
            produtoBanco.Preco = produto.Preco;
            produtoBanco.Fornecedor = produto.Fornecedor;
            
            _context.Produtos.Update(produtoBanco);
            _context.SaveChanges();

            return Ok(produtoBanco);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var produtoBanco = _context.Produtos.Find(id);
            
            if (produtoBanco == null) { return NotFound(); }

            _context.Produtos.Remove(produtoBanco);
            _context.SaveChanges();

            return NoContent();
        }
    }
}