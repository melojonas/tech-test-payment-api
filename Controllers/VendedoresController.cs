using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendedoresController : ControllerBase
    {
        private readonly RegistroContext _context;

        public VendedoresController(RegistroContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Create(PessoaVendedora vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            
            if (vendedor == null) { return NotFound(); }

            return Ok(vendedor);
        }

        
        [HttpGet("ObterPorNome")]
        public IActionResult ObterPorNome(string nome)
        {
            var vendedores = _context.Vendedores.Where(x => x.Nome.Contains(nome));
            
            return Ok(vendedores);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, PessoaVendedora vendedor)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null) { return NotFound(); }

            vendedorBanco.Cpf = vendedor.Cpf;
            vendedorBanco.Nome = vendedor.Nome;
            vendedorBanco.Telefone = vendedor.Telefone;
            vendedorBanco.Email = vendedor.Email;
            
            _context.Vendedores.Update(vendedorBanco);
            _context.SaveChanges();

            return Ok(vendedorBanco);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var vendedorBanco = _context.Vendedores.Find(id);
            
            if (vendedorBanco == null) { return NotFound(); }

            _context.Vendedores.Remove(vendedorBanco);
            _context.SaveChanges();

            return NoContent();
        }
    }
}