using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class RegistroContext : DbContext
    {
        public RegistroContext(DbContextOptions<RegistroContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProdutosPorVenda>()
                .HasOne(p => p.Venda)
                .WithMany(v => v.ProdutosPorVenda)
                .HasForeignKey(p => p.IdVenda)
                .OnDelete(DeleteBehavior.ClientCascade);
        }

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<PessoaVendedora> Vendedores { get; set; }
        public DbSet<ProdutosPorVenda> ProdutosPorVenda { get; set; }
    }
}