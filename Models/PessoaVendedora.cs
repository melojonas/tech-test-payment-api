using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class PessoaVendedora
    {
        [Key]
        public int IdPessoaVendedora { get; set; }

        [RegularExpression(@"/^[0-9]{3}.?[0-9]{3}.?[0-9]{3}-?[0-9]{2}/", 
        ErrorMessage = "CPF inválido. Insira apenas números ou no formato '000.000.000-00'.")]
        public string Cpf { get; set; }

        [Required]
        public string Nome { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [Phone]
        public string Telefone { get; set; }
    }
}