using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Models
{
    public class Produto
    {
        public int Id { get; set; }

        [Required]
        public string Nome { get; set; }
        public string Descricao { get; set; }

        [Required]
        [Column(TypeName = "decimal(19, 2)")]
        public decimal Preco { get; set; }
        public string Fornecedor { get; set; }
    }
}