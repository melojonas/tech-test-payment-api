using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Models
{
    public class ProdutosPorVenda
    {
        public int Id { get; set; }

        [ForeignKey("IdProduto")]
        public Produto Produto { get; set; }
        public int IdProduto { get; set; }

        [ForeignKey("IdVenda")]
        public Venda Venda { get; set; }
        public int IdVenda { get; set; }
    }
}