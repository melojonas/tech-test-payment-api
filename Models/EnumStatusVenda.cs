using System.ComponentModel;

namespace tech_test_payment_api.Models
{
    public enum EnumStatusVenda
    {
        Aguardando,
        Aprovado,
        Enviado,
        Entregue,
        Cancelado
    };
}