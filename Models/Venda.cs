using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusVenda Status { get; set; }
        
        public int IdPessoaVendedora { get; set; }

        [ForeignKey("IdPessoaVendedora")]
        public PessoaVendedora PessoaVendedora { get; set; }

        public List<ProdutosPorVenda> ProdutosPorVenda { get; set; }
    }
}